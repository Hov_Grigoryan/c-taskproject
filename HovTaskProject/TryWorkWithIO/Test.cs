﻿using System;
using System.IO;




namespace TestApplicationForStudy
{
    class Test
    {
        static void Main(string[] args)
        {
            /*  string pathToFile = "C:\\Users\\Hak\\Desktop\\Io\\cities.txt";
              string readAllFile = File.ReadAllText(pathToFile);
              Console.WriteLine(readAllFile);
              string[] readEveryLine = new string[4];
              readEveryLine = File.ReadAllLines(pathToFile);
              for (int i = 0; i < readEveryLine.Length; i++)
              {
                  Console.WriteLine(readEveryLine[i] + " | ");
              }
              File.AppendAllText(pathToFile, Environment.NewLine);
              File.AppendAllText(pathToFile, "Europ");
            *//*  Console.ReadLine();*/



            string pathToDirectory = @"C:\Windows\System32";
            // Получаем пользовательскую дату
            // Будем вводить в формате 27.04.2016 без проверки на корректность ввода
            Console.Write("Write date: ");
            string userDate = Console.ReadLine();
            userDate += " 00:00:00";
            Console.WriteLine("you wrote: {0}", userDate);
            // сохраняем названия всех файлов в переменную allFiles (массив строк)
            DirectoryInfo dI = new DirectoryInfo(pathToDirectory);
            FileInfo[] allFiles = dI.GetFiles();
            try
            {
                // В цикле пройдём по всем файлам и проверим их дату изменения
                foreach (FileInfo fi in allFiles)
                {
                    // Если дата изменения файла старше нашей
                    if (fi.LastWriteTime > DateTime.Parse(userDate))
                    {
                        // выводим этот файл
                        Console.WriteLine("{0} | {1}", fi.Name, fi.LastWriteTime.ToString());
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("it is error: {0}", ex.Message);
            }

            Console.ReadLine();
        }

    }
    }

