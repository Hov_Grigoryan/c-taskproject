﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serializing
{
    [Serializable]
    class Tutorial
    {
        public int Id;
        public String Name;

        public Tutorial(int id, string name)
        {
            Id = id;
            Name = name;
        }

        

    }
}
