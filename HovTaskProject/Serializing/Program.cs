﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Serializing
{
    
    class Program
    {
        static void Main(string[] args)
        {
            Tutorial obj = new Tutorial(1, ".Net");
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(@"C:\Users\Hak\Desktop\Io\Example.txt",FileMode.Create,FileAccess.Write);

            formatter.Serialize(stream, obj);
            stream.Close();

            stream = new FileStream(@"C:\Users\Hak\Desktop\Io\Example.txt", FileMode.Open, FileAccess.Read);
            Tutorial objnew = (Tutorial)formatter.Deserialize(stream);
            Console.WriteLine(objnew.Id);
            Console.WriteLine(objnew.Name);

            Console.ReadKey();
        }
    }
}
