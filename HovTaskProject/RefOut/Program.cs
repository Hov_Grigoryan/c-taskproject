﻿using System;

namespace RefOut
{
    class Program
    {
        static void Main(string[] args)
        {
            int number;
            string input = Console.ReadLine();
            Int32.TryParse(input, out number);

            int SomeFunction(out int x, int y)
            {
                x = 112;
                return x *y;
            }

            int SomeFunction2(in int x, int y)
            {
                
                return x * y;
            }
            int number1 = 45;

            int somInt(ref int pop)
            {
                
                return pop;
            };


            somInt(ref number1);
            Console.WriteLine(number1);
        }
    }
}
