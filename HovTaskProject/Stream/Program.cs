﻿using System;
using System.IO;

namespace Stream
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "C:\\Users\\Hak\\Desktop\\Io\\test.txt";
            if (!File.Exists(path))
            {
                File.Create(path);
            }

            /*for (int i = 0; i < 5; i++)
            {
                File.AppendAllText(path, "barev");
            }*/
            using (StreamReader stream = File.OpenText(path))
            {
                string s = "";
                while ((s=stream.ReadLine()) != null)
                {
                    Console.WriteLine(s);
                    /*stream.Close();*/
                }
            }
            Console.WriteLine(File.ReadAllText(path));
            Console.ReadLine();
        }
    }
}
