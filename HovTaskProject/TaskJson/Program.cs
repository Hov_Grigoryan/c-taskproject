﻿using Newtonsoft.Json;
using System;
using System.Xml;

namespace TaskJson
{
    class Program
    {
        static void Main(string[] args)
        {
            /*var xmlData = "<xmlcontent></xmlcontent>";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlData);
            string jsonText = JsonConvert.SerializeXmlNode(doc);
                

            // Convert Json back to XML
            XmlDocument xmlDoc = JsonConvert.DeserializeXmlNode(@"C:\Users\Hak\Desktop\Io\path.json");
        }*/


            var xml = "<xmlcontent></xmlcontent>";
            string json = @"{
  '?xml': {
    '@version': '1.0',
    '@standalone': 'no'
  },
  'root': {
    'person': [
      {
        '@id': '1',
        'name': 'Alan',
        'url': 'http://www.google.com'
      },
      {
        '@id': '2',
        'name': 'Louis',
        'url': 'http://www.yahoo.com'
      }
    ]
  }
}";
            
            XmlDocument doc = (XmlDocument)JsonConvert.DeserializeXmlNode(json);
            Console.WriteLine(doc.OuterXml);
        }
    }
}
