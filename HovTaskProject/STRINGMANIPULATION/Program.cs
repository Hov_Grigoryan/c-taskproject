﻿using System;

namespace STRINGMANIPULATION
{
    class Program
    {
        static void Main(string[] args)
        {
            /*strmanip();*/
            /*Console.WriteLine(strmanip1("Poxos"));*/
            strmanim2();

            /* strmanim3();*/
            /*strmanim4();*/
            /* strmanip5();*/
            /*strmanip6();*/
            /*strmanip7();*/
        }
        static void strmanip()
        {
            string name;
            string password;
            Console.Write("Enter Username:");
            name = Console.ReadLine();
            Console.Write("Enter Password:");
            password = Console.ReadLine();

            if (name != "")
            {
                if (password.Length >= 8)
                    Console.WriteLine("Welcome!");
                else Console.WriteLine("The password must be greater than 8 characters.");
            }

            else Console.WriteLine("The Username can't be blank.");
        }


        /*ToCharArray*/
        static char[] strmanip1(String s)
        {
            char[] arr = s.ToCharArray();
            foreach (var item in arr)
            {
                Console.WriteLine(item);
            }

            return arr;
        }

        /*Trim*/
        static void strmanim2()
        {
            string st = "C# programming or Csharp programmingC    ";
            Console.WriteLine($"The first occurrence of C character is:   { st.IndexOf('C')}");
            Console.WriteLine($"The last occurrence of C charcter is:   { st.LastIndexOf('C')}");
            Console.WriteLine(st.ToLower());
            Console.WriteLine(st.ToUpper());
            Console.WriteLine(st.Trim());
            Console.WriteLine(st.TrimStart());
            Console.WriteLine(st.TrimEnd());
            string s = st.Substring(4, 5);
            Console.WriteLine(s);
        }

        /*Substring*/
        static void strmanim3()
        {
            string email = "";
            string subemail = "";
            Console.WriteLine("Enter you Email address");
            email = Console.ReadLine();
            int pos = email.IndexOf('@');
            if (pos != -1)
            {
                subemail = email.Substring(email.IndexOf('@'));
            }
            if (subemail.IndexOf('.') != -1)
            {
                Console.WriteLine("Valid email");
            }
            else
            {
                Console.WriteLine("Wrong email");
            }

        }
        /*Split*/
        static void strmanim4()
        {
            /*String str = "Valasasad";
            int v = str.CompareTo("Valod");
            Console.WriteLine(v);*/
            string st = "Hello";
            string[] arrst = st.Split(' ');
            Console.WriteLine();
            foreach (string item in arrst)
                Console.WriteLine($"<{item}>");
        }


        /*Insert*/
        static void strmanip5()
        {
            string st = "C#, C, C++, Java, Python";
            string newst = st.Insert(3, " tutorial");
            Console.WriteLine(newst);
            string[] arrst = st.Split(' ');
            foreach (string item in arrst)
                Console.Write(item);




        }
        /*Remove*/
        static void strmanip6()
        {
            string st = "C#, C, C++, Java, Python programming";
            string tempst = "";
            int pos = st.IndexOf(' ');
            Console.WriteLine(pos);

            while (pos != -1)
            {
                tempst = st.Remove(pos,1);
                st = tempst;
                pos = st.IndexOf(' ');


            }

            Console.WriteLine(st);



        }
        /*Replace*/
        static void strmanip7()
        {


            string st = "C#, C, C++, Java, Python tutorial";
            string newst = st.Replace(",", " tutorial");
            Console.WriteLine(newst);


        }
    }
}

