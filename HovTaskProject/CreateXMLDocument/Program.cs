﻿using System;
using System.IO;
using System.Xml;

namespace CreateXMLDocument
{
    class Program
    {
        static void Main(string[] args)
        {



            XmlDocument doc = new XmlDocument();

            //xml declaration is recommended, but not mandatory
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);

            //create the root element
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            //string.Empty makes cleaner code
            XmlElement element1 = doc.CreateElement(string.Empty, "Mainbody", string.Empty);
            doc.AppendChild(element1);

            XmlElement element2 = doc.CreateElement(string.Empty, "level1", string.Empty);


            XmlElement element3 = doc.CreateElement(string.Empty, "level2", string.Empty);

            XmlText text1 = doc.CreateTextNode("Demo Text");

            element1.AppendChild(element2);
            element2.AppendChild(element3);
            element3.AppendChild(text1);


            XmlElement element4 = doc.CreateElement(string.Empty, "level2", string.Empty);
            XmlText text2 = doc.CreateTextNode("other text");
            element4.AppendChild(text2);
            element2.AppendChild(element4);

            doc.Save(@"C:\Users\Hak\Desktop\Io\document.xml");
            

            XmlDocument xmldoc = new XmlDocument();
            //load XML from the file system URL
            xmldoc.LoadXml(@"C:\Users\Hak\Desktop\Io\document.xml");
        }
    }
}
